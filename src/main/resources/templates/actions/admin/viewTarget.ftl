
<head>
    <meta name="decorator" content="alt.general">
    ${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:progress-bars")}
</head>

[@ui.header pageKey="cloudfoundry.target.info.heading" /]

<div class="aui-group">
    <div class="aui-item">
    	<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-plugin-resources/images/CloudFoundryCorp_vertical_RGB-icon.png" style="float: left; margin-right: 5px" width="80" height="49" />
		${target.name}<br/>
		[@ww.text name='cloudfoundry.global.targets.list.url' /]: <a href="${target.url}" target="_blank">${target.url}</a><br />
		[@ww.text name='cloudfoundry.global.targets.list.credentials' /]: <a id="credentials-tooltip" title="${target.credentials.username}">${target.credentials.name}</a><br />
		[#if target.proxy?has_content]
			[@ww.text name='cloudfoundry.global.targets.list.proxy' /]: <a id="proxy-tooltip" title="${target.proxy.host}:${target.proxy.port}">${target.proxy.name}</a><br />
		[/#if]
		[#if target.disableForBuildPlans]
			[@ww.text name='cloudfoundry.global.targets.list.disableForBuildPlans' /]<br />
		[/#if]	   
    </div>
    <div class="aui-item">
    </div>
    <div class="aui-item">
    </div>
    
</div>

<script type="text/javascript">

    AJS.$(function() {
		AJS.$("#credentials-tooltip").tooltip();
		AJS.$("#proxy-tooltip").tooltip();
    });
</script>

[@dj.tabContainer headingKeys=["cloudfoundry.target.info.basic"] selectedTab='${selectedTab!}']
    [@dj.contentPane labelKey="cloudfoundry.target.info.basic"]
        [@basicTab/]
    [/@dj.contentPane]
[/@dj.tabContainer]

[#macro basicTab]	
	<div class="form-view">
		<div class="field-group">
			<label>Description</label>
			<span class="field-value">${info.description}</span>
		</div>
		<div class="field-group">
			<label>Token Endpoint</label>
			<span class="field-value"><a href="${info.tokenEndpoint}" target="_blank">${info.tokenEndpoint}</a></span>
		</div>
		<div class="field-group">
			<label>Authorization Endpoint</label>
			<span class="field-value"><a href="${info.authorizationEndpoint}" target="_blank">${info.authorizationEndpoint}</a></span>
		</div>
		<div class="field-group">
			<label>Support</label>
			<span class="field-value"><a href="${info.support}" target="_blank">${info.support}</a></span>
		</div>
		<div class="field-group">
			<label>API Version</label>
			<span class="field-value">${info.apiVersion}</span>
		</div>
		<div class="field-group">
			<label>Minimum CLI Version</label>
			<span class="field-value">${info.minCliVersion}</span>
		</div>
		<div class="field-group">
			<label>Recommended Minimum CLI Version</label>
			<span class="field-value">${info.minRecommendedCliVersion}</span>
		</div>
	</div>
[/#macro]