[#-- @ftlvariable name="action" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageProxiesAction" --]
[#-- @ftlvariable name="" type="org.gaptap.bamboo.cloudfoundry.admin.actions.ManageProxiesAction" --]

[#if mode == 'edit' ]
	[#assign targetAction = "/admin/cloudfoundry/updateProxy.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.proxy.button.update"]
[#else]
	[#assign targetAction = "/admin/cloudfoundry/createProxy.action"]
	[#assign submitButtonKey = "cloudfoundry.admin.proxy.button.create"]
[/#if]

<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.admin.proxy.heading" /]
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey=submitButtonKey
			titleKey='cloudfoundry.admin.proxy.form.title'
			cancelUri='/admin/cloudfoundry/configuration.action'
			showActionErrors='true'
			descriptionKey='cloudfoundry.admin.proxy.form.description']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='proxyId' /]
		[/#if]
		[@ww.textfield name='name' labelKey='cloudfoundry.proxy.name' descriptionKey='cloudfoundry.proxy.name.description' required='true' /]
		[@ww.textfield name='host' labelKey='cloudfoundry.proxy.host' descriptionKey='cloudfoundry.proxy.host.description' required='true' /]
		[@ww.textfield name='port' labelKey='cloudfoundry.proxy.port' descriptionKey='cloudfoundry.proxy.port.description' required='true' /]
		[@ww.textfield name='description' labelKey='cloudfoundry.proxy.description' descriptionKey='cloudfoundry.proxy.description.description' required='false' /]
	[/@ww.form]
</body>
</html>
