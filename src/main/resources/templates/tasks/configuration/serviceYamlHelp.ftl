[#ftl]
<html>
<head>
    <title>Cloud Foundry service.yml Help</title>
    <meta name="decorator" content="atl.popup">
</head>

<body>
	[@ui.header page="Supported Cloud Foundry Service Manifest File Options" /]
	[@ui.bambooSection title='Minimum Fields']
	<p>At a minimum, the following fields must be provided.</p>
	<br/>
	<pre class="code">
---
services:
- service_instance: my-rabbit-service
  service: p-rabbitmq
  plan: standard
	</pre>


	<br/>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Tags and parameters']
		<p>Tags and parameters are supported.</p>
		<br/>
		<pre class="code">
---
services:
- service_instance: my-service
  service: p-rabbitmq
  plan: standard
  tags:
    - tag1
    - tag2
  parameters_json: >
     {
        "some" : "thing",
        "something" : "else"
     }
		</pre>
	<br/>

		<p>Instead of a <span class="code">parameters_json</span> field, a <span class="code">parameters</span> field is also supported.</p>
		<br/>
				<pre class="code">
---
services:
- service_instance: my-service
  service: p-rabbitmq
  plan: standard
  tags:
    - tag1
    - tag2
  parameters:
    some : thing
    something: else
        		</pre>
	[/@ui.bambooSection]
	[@ui.bambooSection title='Multiple Services']
		<p>Multiple services can be managed via a single manifest</p>
		<br/>
		<pre class="code">
---
services:
- service_instance: my-rabbit-service
  service: p-rabbitmq
  plan: standard
- service_instance: my-mysql-service
  service: mysql
  plan: small
		</pre>
	<br/>
	[/@ui.bambooSection]
</body>
</html>