/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import com.atlassian.bamboo.security.EncryptionService;
import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.DefaultCloudFoundryOperations;
import org.cloudfoundry.reactor.DefaultConnectionContext;
import org.cloudfoundry.reactor.ProxyConfiguration;
import org.cloudfoundry.reactor.client.ReactorCloudFoundryClient;
import org.cloudfoundry.reactor.doppler.ReactorDopplerClient;
import org.cloudfoundry.reactor.tokenprovider.PasswordGrantTokenProvider;
import org.cloudfoundry.reactor.uaa.ReactorUaaClient;
import org.cloudfoundry.uaa.UaaClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

/**
 * @author David Ehringer
 * 
 */
public class DefaultCloudFoundryServiceFactory implements CloudFoundryServiceFactory {

    private final ConnectionValidator validator = new ConnectionValidator();
    private final EncryptionService encryptionService;
    private HealthChecker healthChecker;

    public DefaultCloudFoundryServiceFactory(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
        this.healthChecker = new ReactorHealthChecker();
    }

    /**
     * This is primarily for testing purposes. Optimally, we would like to inject the HealthChecker into
     * the constructor. But that only works on local agents and not remote agents.
     */
    void setHealthChecker(HealthChecker healthChecker) {
        this.healthChecker = healthChecker;
    }

    @Override
    public CloudFoundryService getCloudFoundryService(ConnectionParameters connectionParameters, Logger logger)
            throws CloudFoundryServiceException, InvalidUrlException {
        return build(connectionParameters, null, null, logger);
    }

    @Override
    public CloudFoundryService getCloudFoundryService(ConnectionParameters connectionParameters, String org,
                                                      String space, Logger logger) throws CloudFoundryServiceException, InvalidUrlException {
        return build(connectionParameters, org, space, logger);
    }

    private CloudFoundryService build(ConnectionParameters connectionParameters, String org,
                                      String space, Logger logger){
        DefaultConnectionContext connectionContext = buildDefaultConnectionContext(connectionParameters);

        String password = connectionParameters.getPassword();
        if(connectionParameters.isPasswordEncrypted()){
            password = encryptionService.decrypt(password);
        }
        PasswordGrantTokenProvider tokenProvider = PasswordGrantTokenProvider.builder()
                .username(connectionParameters.getUsername())
                .password(password)
                .build();

        UaaClient uaaClient = ReactorUaaClient.builder()
                .connectionContext(connectionContext)
                .tokenProvider(tokenProvider)
                .build();

        CloudFoundryClient cloudFoundryClient = ReactorCloudFoundryClient.builder()
                .connectionContext(connectionContext)
                .tokenProvider(tokenProvider)
                .build();

        DopplerClient dopplerClient = ReactorDopplerClient.builder()
                .connectionContext(connectionContext)
                .tokenProvider(tokenProvider)
                .build();

        CloudFoundryOperations cloudFoundryOperations = DefaultCloudFoundryOperations.builder()
                .cloudFoundryClient(cloudFoundryClient)
                .dopplerClient(dopplerClient)
                .uaaClient(uaaClient)
                .organization(org)
                .space(space)
                .build();

        CloudFoundryService cloudFoundryService = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);
        validate(cloudFoundryService);
        return cloudFoundryService;
    }

    private DefaultConnectionContext buildDefaultConnectionContext(ConnectionParameters connectionParameters) {
        DefaultConnectionContext.Builder connectionContextBuilder = DefaultConnectionContext.builder()
                .apiHost(getApiHost(connectionParameters))
                .skipSslValidation(connectionParameters.isTrustSelfSignedCerts());
        if(connectionParameters.getProxyHost() != null) {
            connectionContextBuilder
                    .proxyConfiguration(ProxyConfiguration.builder()
                            .host(connectionParameters.getProxyHost())
                            .port(connectionParameters.getProxyPort())
                            .build());
        }

        DefaultConnectionContext connectionContext = null;
        try {
            connectionContext = connectionContextBuilder.build();
        } catch (Exception e){
            // connectionContextBuilder.build can throw UnknownHostException bubbles up as a reactor.core.Exceptions$ReactiveException
            throw new InvalidUrlException(e);
        }
        return connectionContext;
    }

    private String getApiHost(ConnectionParameters connectionParameters) {
        try {
            URI uri = new URI(connectionParameters.getTargetUrl());
            return uri.getHost();
        } catch (URISyntaxException e) {
            throw new InvalidUrlException(e);
        }
    }

    private void validate(CloudFoundryService cloudFoundryService) {
        if(!validator.isValid(cloudFoundryService)){
            throw new CloudFoundryServiceException("Invalid connection information. Please verify the API URL, username, and/or password.");
        }
    }
}
