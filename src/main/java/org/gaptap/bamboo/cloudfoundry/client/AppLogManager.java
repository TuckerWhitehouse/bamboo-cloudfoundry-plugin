/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.doppler.Envelope;
import org.cloudfoundry.doppler.EventType;
import org.cloudfoundry.doppler.LogMessage;
import org.cloudfoundry.doppler.StreamRequest;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class AppLogManager {

    private final CloudFoundryOperations cloudFoundryOperations;
    private final DopplerClient dopplerClient;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm.ssZZ");

    private Disposable disposable;

    public AppLogManager(CloudFoundryOperations cloudFoundryOperations, DopplerClient dopplerClient) {
        this.cloudFoundryOperations = cloudFoundryOperations;
        this.dopplerClient = dopplerClient;
    }

    public AppLogManager(CloudFoundryOperations cloudFoundryOperations, DopplerClient dopplerClient, TimeZone timeZone) {
        this(cloudFoundryOperations, dopplerClient);
        dateFormat.setTimeZone(timeZone);
    }

    public Mono<Void> startTailingLogs(String applicationName, Logger logger){
        disposable = cloudFoundryOperations
                .applications()
                        .get(GetApplicationRequest.builder()
                                .name(applicationName)
                                .build())
                .flatMap(applicationDetail -> {
                    return dopplerClient
                            .stream(StreamRequest.builder()
                                    .applicationId(applicationDetail.getId())
                                    .build())
                            .subscribeOn(Schedulers.elastic());
                })
                .subscribe(
                        envelope -> processEvent(envelope, logger),
                        throwable -> logger.error("Error occurred while tailing application logs: " + throwable.getMessage())
                );
        return Mono.empty();
    }

    private void processEvent(Envelope envelope, Logger logger) {
        if (EventType.LOG_MESSAGE == envelope.getEventType()) {
            LogMessage logMessage = envelope.getLogMessage();

            StringBuilder message = new StringBuilder();

            message.append(formatTimestamp(logMessage));
            message.append(" ");
            message.append(formatPrefix(logMessage));
            message.append(logMessage.getMessageType());
            message.append(" ");
            message.append(logMessage.getMessage());

            logger.info(message.toString());
        }
    }

    private String formatTimestamp(LogMessage logMessage) {
        long timestamp = TimeUnit.MILLISECONDS.convert(logMessage.getTimestamp(), TimeUnit.NANOSECONDS);
        return dateFormat.format(new Date(timestamp));
    }

    private String formatPrefix(LogMessage logMessage) {
        return String.format("%1$-13s", String.format("[%s/%s]", logMessage.getSourceType(), logMessage.getSourceInstance()));
    }

    public Mono<Void> stopTailingLogs(){
        if(disposable != null) {
            disposable.dispose();
        }
        return Mono.empty();
    }
}
