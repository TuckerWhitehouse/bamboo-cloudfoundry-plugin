/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.apache.commons.lang.StringUtils;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Value.Immutable
abstract class _BlueGreenConfiguration {

    @Value.Default
    @Nullable
    public Boolean enabled() {
        return false;
    }

    @Value.Derived
    public boolean isEnabled(){
        if(enabled() == null){
            return false;
        }
        return enabled();
    }

    @Value.Default
    public boolean useCustomDarkAppConfiguration() {
        return false;
    }

    @Nullable
    abstract String customDarkAppName();

    @Value.Derived
    public boolean useCustomDarkAppName(){
        return customDarkAppName() != null;
    }

    @Nullable
    abstract String customDarkRoute();

    @Value.Derived
    public boolean useCustomDarkRoute(){
        return customDarkRoute() != null;
    }

    @Nullable
    abstract String healthCheckEndpoint();

    @Value.Derived
    public boolean healthCheckEnabled(){
        return StringUtils.isNotBlank(healthCheckEndpoint());
    }

    @Value.Default
    @Nullable
    public Boolean skipSslValidation(){
        return false;
    }

    @Value.Check
    protected void check() {
        if(!useCustomDarkAppConfiguration() && (useCustomDarkAppName() || useCustomDarkRoute())){
            throw new IllegalArgumentException("If useCustomDarkAppConfiguration == false, customDarkAppName and customDarkRoute cannot be provided.");
        }
    }

    public void logConfiguration(Logger logger) {
        logger.info("--- Blue/green Deployment Configuration ---");
        logger.info("enabled: " + isEnabled());
        if(enabled()) {
            logger.info("custom dark app name: " + valueOrEmpty(customDarkAppName()));
            logger.info("custom dark app route: " + valueOrEmpty(customDarkRoute()));
            logger.info("health check enabled: " + valueOrEmpty(healthCheckEnabled()));
            logger.info("health check endpoint: " + valueOrEmpty(healthCheckEndpoint()));
            logger.info("skip SSL validation: " + valueOrEmpty(skipSslValidation()));
        }
        logger.info("-------------------------------------------");
    }

    private String valueOrEmpty(Object field) {
        return field == null ? "" :String.valueOf(field);
    }

    public ApplicationConfiguration convertToDarkAppConfiguration(ApplicationConfiguration applicationConfiguration){
        ApplicationConfiguration.Builder builder = ApplicationConfiguration.builder()
                .from(applicationConfiguration);
        if(enabled()) {
            builder.name(convertName(applicationConfiguration));
            builder.routes(convertRoutes(applicationConfiguration));
        }
        return builder.build();
    }

    private String convertName(ApplicationConfiguration applicationConfiguration) {
        if(useCustomDarkAppName()){
            return customDarkAppName();
        }
        return conventionBasedDarkAppName(applicationConfiguration);
    }

    private String conventionBasedDarkAppName(ApplicationConfiguration applicationConfiguration) {
        return applicationConfiguration.name() + "-dark";
    }

    private List<String> convertRoutes(ApplicationConfiguration applicationConfiguration) {
        if (useCustomDarkRoute()) {
            return Arrays.asList(customDarkRoute());
        }
        return conventionBasedDarkRoutes(applicationConfiguration);
    }

    private List<String> conventionBasedDarkRoutes(ApplicationConfiguration applicationConfiguration) {
        List<String> darkRoutes = new ArrayList<>();
        applicationConfiguration.routes().stream()
                .map(this::convertToDarkRoute)
                .forEach(darkRoutes::add);
        return darkRoutes;
    }

    private String convertToDarkRoute(String route) {
        MapRouteRequest mapRouteRequest = RouteRequestBuilder.buildMapRouteRequest("n/a", route);
        StringBuilder darkRoute = new StringBuilder();
        darkRoute.append(mapRouteRequest.getHost());
        darkRoute.append("-dark.");
        darkRoute.append(mapRouteRequest.getDomain());
        if(mapRouteRequest.getPath() != null){
            darkRoute.append("/");
            darkRoute.append(mapRouteRequest.getHost());
        }
        return darkRoute.toString();
    }
}
