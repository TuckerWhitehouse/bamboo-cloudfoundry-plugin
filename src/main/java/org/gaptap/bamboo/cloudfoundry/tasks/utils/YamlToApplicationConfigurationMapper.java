/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.RandomStringUtils;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.Logger;
import org.gaptap.bamboo.cloudfoundry.client.ServiceManifest;
import org.springframework.util.PropertyPlaceholderHelper;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration.DEFAULT_INSTANCE_COUNT;
import static org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration.DEFAULT_MEMORY;

/**
 * Provides limited support for the manifest.yml format. Would prefer not to
 * support this long term but this isn't yet natively supported by the
 * vcap_java_client.
 * <p/>
 * For variable substitution inside the <code>applications</code> block, the
 * variable value must be available before it can be referenced. The way this is
 * done isn't so great so ordering of the manifest is significant in successful
 * variable substitution. :-(
 * <p/>
 * This class is stateful and not thread-safe.
 * 
 * @author David Ehringer
 */
public class YamlToApplicationConfigurationMapper {

    // TODO Byte quantity must be an integer with a unit of measurement like M, MB, G, or GB
    private static final Pattern QUOTA_PATTERN = Pattern.compile("\\d+");
    private static final Pattern TARGET_BASE_PATTERN = Pattern
            .compile("^[^\\.]+\\.(.*)");

    private final PropertyPlaceholderHelper resolver = new PropertyPlaceholderHelper(
            "${", "}");
    private final String defaultDomain;

    private final Properties properties = new Properties();

    private final Logger logger;

    public YamlToApplicationConfigurationMapper(String defaultDomain, Logger logger) {
        this.defaultDomain = defaultDomain;
        this.logger = logger;
        init();
    }

    private void init() {
        properties.put("target-base", defaultDomain);
    }

    private void reset() {
        properties.clear();
        init();
    }

    @SuppressWarnings("rawtypes")
    public ApplicationConfiguration from(InputStream input,
            Map<String, String> bambooVariables) {
        reset();
        addBambooVariablesForResolution(bambooVariables);

        Map manifest = readManifest(input);
        addTopLevelAttributesForResolution(manifest);

        Map app = getApp(manifest);
        warnIfPathSpecified(app);

        return ApplicationConfiguration.builder()
                .name(extractField(app, "name"))
                .instances(extractInstances(app))
                .memory(extractMemory(app))
                .diskQuota(extractDiskQuota(app))
                .addAllHosts(extractHosts(app))
                .addAllDomains(extractDomains(app))
                .addAllRoutes(extractUrls(app))
                .addAllRoutes(extractRoutes(app))
                .noHostname(extractBoolean(app, "no-hostname", false))
                .noRoute(extractBoolean(app, "no-route"))
                .buildpackUrl(extractField(app, "buildpack"))
                .command(extractField(app, "command"))
                .healthCheckTimeout(extractInteger(app, "timeout"))
                .putAllEnvironment(extractEnv(app))
                .addAllServiceBindings(extractServiceBindings(app))
                .stack(extractField(app, "stack"))
                .healthCheckType(extractField(app, "health-check-type"))
                .build();
    }

    private void addBambooVariablesForResolution(
            Map<String, String> bambooVariables) {
        properties.putAll(bambooVariables);
    }

    @SuppressWarnings("rawtypes")
    private void addTopLevelAttributesForResolution(Map manifest) {
        for (Object obj : manifest.keySet()) {
            String key = (String) obj;
            if (!"applications".equals(key)) {
                Object rawValue = manifest.get(key);
                if (rawValue instanceof String || rawValue instanceof Number) {
                    properties.put(key,
                            resolveSymbols(String.valueOf(rawValue)));
                } else {
                    throw new IllegalArgumentException("Invalid value for "
                            + key + ". Value must be a String or Number.");
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
    private Map readManifest(InputStream input) {
        Yaml yaml = new Yaml();
        return (Map) yaml.load(input);
    }

    /**
     * Extracts a String field from a Map, resolves any placeholders, and then
     * makes the field value available for future placeholder resolution.
     */
    @SuppressWarnings("rawtypes")
    private String extractField(Map map, String fieldName) {
        String rawValue = (String) map.get(fieldName);
        if (rawValue != null) {
            String resolvedValue = resolveSymbols(rawValue);
            properties.put(fieldName, resolvedValue);
            return resolvedValue;
        }
        return null;
    }

    private Boolean extractBoolean(Map map, String fieldName, boolean defaultWhenNull){
        Boolean result = extractBoolean(map, fieldName);
        if(result == null){
            result = defaultWhenNull;
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    private Boolean extractBoolean(Map map, String fieldName) {
        Object rawValue = map.get(fieldName);
        if (rawValue != null) {
            Boolean value = null;
            if (rawValue instanceof Boolean) {
                value = (Boolean) rawValue;
            }  else {
                throw new IllegalArgumentException(fieldName
                        + " must be a boolean value." + rawValue
                        + " was provided.");
            }

            properties.put(fieldName, String.valueOf(value));
            return value;
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    private Integer extractInteger(Map map, String fieldName) {
        Object rawValue = map.get(fieldName);
        if (rawValue != null) {
            Integer value = null;
            if (rawValue instanceof String) {
                value = Integer.valueOf(resolveSymbols((String) rawValue));
            } else if (rawValue instanceof Integer) {
                value = (Integer) rawValue;
            } else {
                throw new IllegalArgumentException(fieldName
                        + " must be an integer value." + rawValue
                        + " was provided.");
            }

            properties.put(fieldName, String.valueOf(value));
            return value;
        }
        return null;
    }

    private String resolveSymbols(String value) {
        value = value.replace("${random-word}",
                RandomStringUtils.random(4, true, false));
        return resolver.replacePlaceholders(value, properties);
    }

    @SuppressWarnings("rawtypes")
    private Map getApp(Map map) {
        if (map.get("applications") instanceof List) {
            List applications = (List) map.get("applications");
            if (applications.size() != 1) {
                throw new IllegalArgumentException(
                        "Expecting a single application entry in the manifest file. Found "
                                + applications.size());
            }
            return (Map) applications.get(0);
        } else if (map.get("applications") instanceof Map) {
            logger.warn("\n************ Deprecation Warning ************\n");
            logger.warn("\tManifests with applications defined as a YAML map rather than a YAML list are deprecated and will be removed in a future version. Please use the standard manifest format instead:\n");
            logger.warn("\t---\n" +
                    "applications:\n" +
                    "- name: example-manifest");
            logger.warn("\n*********************************************\n");
            Map applications = (Map) map.get("applications");
            // this is... interesting
            return (Map) ((Entry) applications.entrySet().iterator().next())
                    .getValue();
        } else {
            throw new IllegalArgumentException(
                    "Manifest file is not in an acceptable format. Unknown 'applications' structure.");
        }
    }

    private Integer extractInstances(Map app) {
        Integer instances = extractInteger(app, "instances");
        if(instances == null){
            instances = DEFAULT_INSTANCE_COUNT;
        }
        return instances;
    }

    @SuppressWarnings("rawtypes")
    private Integer extractMemory(Map app) {
        String memory = extractField(app, "memory");
        if (memory == null) {
            memory = extractDeprecatedMemField(app);
        }
        if(memory == null){
            return DEFAULT_MEMORY;
        }
        Matcher m = QUOTA_PATTERN.matcher(memory);
        if (m.find()) {
            int memoryQuota = Integer.parseInt(m.group());
            if(isUnitGb(memory, "memory")){
                memoryQuota = memoryQuota * 1024;
            }
            return memoryQuota;
        }
        throw new IllegalArgumentException("Invalid memory value: " + memory);
    }

    private String extractDeprecatedMemField(Map app) {
        String memory = extractField(app, "mem");
        if(memory != null){
            logger.warn("\n************ Deprecation Warning ************\n");
            logger.warn("\tManifest attribute 'mem' is deprecated and will be removed in a future version. Please use 'memory' instead.\n");
            logger.warn("*********************************************\n");
        }
        return memory;
    }

    @SuppressWarnings("rawtypes")
    private int extractDiskQuota(Map app) {
        // This is a bit messy
        String disk = null;
        try {
            disk = extractField(app, "disk_quota");
            if(disk == null) {
                disk = extractDeprecatedDiskQuota(app);
            }
        } catch (ClassCastException e) {
            // this handles the case where snakeyml has bound disk to an Integer
            // instead of String because it doesn't have a suffix
            throw new IllegalArgumentException(
                    "Disk quota value must end with either M or G for megabytes and gigabytes respectively. For example 1024M or 1GB.");
        }
        if (disk == null) {
            return ApplicationConfiguration.DEFAULT_DISK_QUOTA;
        }
        Matcher m = QUOTA_PATTERN.matcher(disk);
        if (m.find()) {
            int diskQuota = Integer.parseInt(m.group());
            if(isUnitGb(disk, "disk_quota")){
                diskQuota = diskQuota * 1024;
            }
            return diskQuota;
        }
        throw new IllegalArgumentException("Invalid disk_quota value: " + disk);
    }

    private boolean isUnitGb(String value, String field) {
        if (value.toUpperCase().endsWith("G") || value.toUpperCase().endsWith("GB")) {
            return true;
        } else if (!value.toUpperCase().endsWith("M") && !value.toUpperCase().endsWith("MB")) {
            throw new IllegalArgumentException(
                    field + " value requires a unit of measurement: M, MB, G, or GB, in upper case or lower case. For example 1024M or 1GB. Provided value: "
                            + value);
        }
        return false;
    }

    private String extractDeprecatedDiskQuota(Map app) {
        String disk = extractField(app, "disk");
        if(disk != null){
            logger.warn("\n************ Deprecation Warning ************\n");
            logger.warn("\tManifest attribute 'disk' is deprecated and will be removed in a future version. Please use 'disk_quota' instead.\n");
            logger.warn("*********************************************\n");
        }
        return disk;
    }

    @SuppressWarnings("rawtypes")
    private Map<String, String> extractEnv(Map app) {
        Map envString = (Map) app.get("env");
        Map<String, String> env = Maps.newHashMap();
        if (envString != null) {
            for (Object key : envString.keySet()) {
                String name = resolveSymbols(String.valueOf(key));
                String value = resolveSymbols(String
                        .valueOf(envString.get(key)));
                env.put(name, value);
            }
        }
        return env;
    }

    private List<String> extractDomains(Map app) {
        List<String> domains = Lists.newArrayList();

        String extractedHost = extractField(app, "domain");
        if(extractedHost != null) {
            domains.add(extractedHost);
        }

        if((List<String>)app.get("domains") != null){
            domains.addAll((List<String>) app.get("domains"));
        }

        if(shouldAddDefaultDomain(domains, app)){
            domains.add(defaultDomain);
        }
        return domains;
    }

    private boolean shouldAddDefaultDomain(List<String> domains, Map app) {
        return domains.isEmpty() && !extractHosts(app).isEmpty();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private List<String> extractHosts(Map app) {
        List<String> hosts = Lists.newArrayList();

        String extractedHost = extractField(app, "host");
        if(extractedHost != null) {
            hosts.add(extractedHost);
        }

        if((List<String>)app.get("hosts") != null){
            hosts.addAll((List<String>)app.get("hosts"));
        }
        return hosts;
    }

    @SuppressWarnings("rawtypes")
    private List<String> extractUrls(Map app) {
        List<String> urls = Lists.newArrayList();
        parseUrls(app.get("url"), urls);
        parseUrls(app.get("urls"), urls);
        return urls;
    }

    @SuppressWarnings("unchecked")
    private void parseUrls(Object urlField, List<String> urls) {
        if (urlField != null) {
            logger.warn("\n************ Deprecation Warning ************\n");
            logger.warn("\tManifest attributes 'url' and 'urls' are deprecated and will be removed in a future version. Please use 'routes' instead. Please see https://docs.cloudfoundry.org/devguide/deploy-apps/manifest.html#routes.\n");
            logger.warn("*********************************************\n");
            if (urlField instanceof String) {
                urls.add(resolveSymbols((String) urlField));
            } else if (urlField instanceof List) {
                for (String url : (List<String>) urlField) {
                    urls.add(resolveSymbols(url));
                }
            }
        }
    }

    private List<String> extractRoutes(Map app) {
        List<String> routes = Lists.newArrayList();
        Object routesAttr = app.get("routes");
        if(routesAttr != null) {
            if (routesAttr instanceof List) {
                for (Object route : (List<Object>) routesAttr) {
                    routes.add(extractRoute(route));
                }
            } else {
                throw new IllegalArgumentException("Invalid format for 'routes'");
            }
        }
        return routes;
    }

    private String extractRoute(Object route) {
        if (route instanceof Map){
            Object routeValue = ((Map)route).get("route");
            if(!(routeValue instanceof String)){
                throw new IllegalArgumentException("Invalid format for 'route'");
            }
            return resolveSymbols((String) routeValue);
        } else {
            throw new IllegalArgumentException("Invalid format for 'route'");
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private List<ServiceManifest> extractServiceManifests(Map app) {
        List<ServiceManifest> services = Lists.newArrayList();
        if (app.get("services") instanceof Map) {
            Map map = (Map) app.get("services");
            for (Object service : map.keySet()) {
                ServiceManifest.Builder builder = ServiceManifest.builder();

                String name = resolveSymbols(String.valueOf(service));
                builder.name(name);

                Map<Object, Object> serviceDetails = (Map<Object, Object>) map
                        .get(service);
                // These can be repeated if there are multiple services so it
                // doesn't make sense to make them available for replacement
                builder.plan(String.valueOf(serviceDetails.get("plan")));
                builder.label(String.valueOf(serviceDetails.get("label")));

                services.add(builder.build());
            }
        }

        return services;
    }

    @SuppressWarnings("rawtypes")
    private List<String> extractServiceBindings(Map app) {
        List<String> services = Lists.newArrayList();
        if (app.get("services") instanceof List) {
            List serviceList = (List) app.get("services");
            for (Object service : serviceList) {
                String name = resolveSymbols(String.valueOf(service));
                services.add(name);
            }
        }
        return services;
    }

    private void warnIfPathSpecified(Map app) {
        String path = extractField(app, "path");
        if(path != null){
            logger.warn("WARNING: 'path' attribute found in manifest. It's value will be ignored in favor of the path specified in the task configuration.");
        }
    }
}
