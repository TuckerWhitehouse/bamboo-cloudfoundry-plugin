/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;

/**
 * @author David Ehringer
 */
public class RoutingTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String OPTIONS = "cf_options";
    public static final String SELECTED_OPTION = "cf_option";
    public static final String OPTION_ADD_DOMAIN = "addDomain";
    public static final String OPTION_DELETE_DOMAIN = "deleteDomain";
    public static final String OPTION_ADD_ROUTE = "addRoute";
    public static final String OPTION_DELETE_ROUTE = "deleteRoute";
    public static final String OPTION_BIND_ROUTE_SERVICE = "bindRouteService";

    public static final String DOMAIN_ADD = "cf_addDomain";
    public static final String DOMAIN_DELETE = "cf_deleteDomain";

    public static final String DOMAIN_ROUTE_ADD = "cf_domain_addRoute";
    public static final String HOST_ROUTE_ADD = "cf_host_addRoute";
    public static final String PATH_ROUTE_ADD = "cf_path_addRoute";

    public static final String DOMAIN_ROUTE_DELETE = "cf_domain_deleteRoute";
    public static final String HOST_ROUTE_DELETE = "cf_host_deleteRoute";
    public static final String PATH_ROUTE_DELETE = "cf_path_deleteRoute";

    public static final String SERVICE_NAME_BIND_SERVICE = "cf_serviceName_bindRouteService";
    public static final String DOMAIN_BIND_SERVICE = "cf_domain_bindRouteService";
    public static final String HOST_BIND_SERVICE = "cf_host_bindRouteService";
    public static final String PATH_BIND_SERVICE = "cf_path_bindRouteService";
    public static final String SELECTED_DATA_OPTION = "cf_dataOption_bindRouteService";
    public static final String DATA_OPTION_INLINE = "inline";
    public static final String DATA_OPTION_FILE = "file";
    public static final String JSON_PARAMS_INLINE = "cf_inlineData_bindRouteService";
    public static final String JSON_PARAMS_FILE = "cf_file_bindRouteService";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, DOMAIN_ADD, DOMAIN_DELETE,
            DOMAIN_ROUTE_ADD, HOST_ROUTE_ADD, PATH_ROUTE_ADD, DOMAIN_ROUTE_DELETE, HOST_ROUTE_DELETE, PATH_ROUTE_DELETE,
            SERVICE_NAME_BIND_SERVICE, DOMAIN_BIND_SERVICE, HOST_BIND_SERVICE, PATH_BIND_SERVICE, SELECTED_DATA_OPTION, JSON_PARAMS_INLINE, JSON_PARAMS_FILE) ;

    public RoutingTaskConfigurator(CloudFoundryAdminService adminService,
                                   TextProvider textProvider,
                                   TaskConfiguratorHelper taskConfiguratorHelper,
                                   EncryptionService encryptionService,
                                   CloudFoundryServiceFactory cloudFoundryServiceFactory) {
        super(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);

        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);

        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);

    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> options = new LinkedHashMap<String, String>();
        options.put(OPTION_ADD_DOMAIN, textProvider.getText("cloudfoundry.task.route.option.addDomain"));
        options.put(OPTION_DELETE_DOMAIN, textProvider.getText("cloudfoundry.task.route.option.deleteDomain"));
        options.put(OPTION_ADD_ROUTE, textProvider.getText("cloudfoundry.task.route.option.addRoute"));
        options.put(OPTION_DELETE_ROUTE, textProvider.getText("cloudfoundry.task.route.option.deleteRoute"));
        options.put(OPTION_BIND_ROUTE_SERVICE, textProvider.getText("cloudfoundry.task.route.option.bindRouteService"));
        context.put(OPTIONS, options);

        Map<String, String> dataOptions = new LinkedHashMap<>();
        dataOptions.put(DATA_OPTION_INLINE, textProvider.getText("cloudfoundry.task.userservice.data.options.inline"));
        dataOptions.put(DATA_OPTION_FILE, textProvider.getText("cloudfoundry.task.userservice.data.options.file"));
        context.put("dataOptionsBindRouteService", dataOptions);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String option = params.getString(SELECTED_OPTION);

        if (OPTION_ADD_DOMAIN.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ADD, params, errorCollection);
        } else if (OPTION_DELETE_DOMAIN.equals(option)) {
            validateRequiredNotBlank(DOMAIN_DELETE, params, errorCollection);
        } else if (OPTION_ADD_ROUTE.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ROUTE_ADD, params, errorCollection);
            validateRequiredNotBlank(HOST_ROUTE_ADD, params, errorCollection);
            validatePath(params, PATH_ROUTE_ADD, errorCollection);
        } else if (OPTION_DELETE_ROUTE.equals(option)) {
            validateRequiredNotBlank(DOMAIN_ROUTE_DELETE, params, errorCollection);
            validateRequiredNotBlank(HOST_ROUTE_DELETE, params, errorCollection);
            validatePath(params, PATH_ROUTE_DELETE, errorCollection);
        } else if (OPTION_BIND_ROUTE_SERVICE.equals(option)) {
            validateRequiredNotBlank(SERVICE_NAME_BIND_SERVICE, params, errorCollection);
            validateRequiredNotBlank(DOMAIN_BIND_SERVICE, params, errorCollection);
            validatePath(params, PATH_ROUTE_DELETE, errorCollection);
        } else {
            errorCollection.addError(SELECTED_OPTION, textProvider.getText("cloudfoundry.task.route.option.unknown"));
        }
    }

    private void validatePath(ActionParametersMap params, String pathParam, ErrorCollection errorCollection) {
        String path = params.getString(pathParam);
        if(StringUtils.isNotEmpty(path) && !containsBambooVariable(path)){
            if(!path.startsWith("/")){
                errorCollection.addError(pathParam, textProvider.getText("cloudfoundry.task.routing.path.invalid"));
            }
        }
    }
}
