/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author David Ehringer
 */
public class TableBuilderTest {

    @Test
    public void test(){
        TableBuilder tableBuilder = new TableBuilder();
        tableBuilder.addRow("", "state", "since", "cpu", "memory", "disk", "details");
        tableBuilder.addRow("#0", "running", "2016-08-17 04:26:10 PM", "0.0%", "6.8M of 1G", "8.7M of 1G");

        assertThat(tableBuilder.getTable().get(0), is("     state     since                    cpu    memory       disk         details   "));
        assertThat(tableBuilder.getTable().get(1), is("#0   running   2016-08-17 04:26:10 PM   0.0%   6.8M of 1G   8.7M of 1G   "));
    }

}
