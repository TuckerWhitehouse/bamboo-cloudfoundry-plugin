package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import org.cloudfoundry.client.v3.tasks.CreateTaskResponse;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_RUN_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_WAIT_ON_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_CAPTURE_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_ID_VARIABLE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_FAIL_ON_TASK_FAILURE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TERMINATE_TASK_ON_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TIMEOUT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TaskTaskTest extends AbstractTaskTest{

    private TaskTask task;

    @Before
    public void init() {
        createDefaultConfigMap();

        task = new TaskTask(encryptionService);
        task.setCloudFoundryServiceFactory(cloudFoundryServiceFactory);
    }

    private void createDefaultConfigMap() {
        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
    }

    private Mono<CreateTaskResponse> createTaskResponse(){
        return createTaskResponse(UUID.randomUUID().toString());
    }

    private Mono<CreateTaskResponse> createTaskResponse(String id){
        return Mono
                .just(CreateTaskResponse.builder()
                        .id(id)
                        .build());
    }

    @Test
    public void runTaskWithMinimumParameters() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_RUN_TASK);
        configurationMap.put(RUN_APPLICATION_NAME, "test-app");
        configurationMap.put(RUN_COMMAND, "start app");
        configurationMap.put(RUN_TASK_NAME, "my-task");

        when(cloudFoundryService.runTask(anyString(), anyString(), anyString(), anyInt(), anyMap()))
                .thenReturn(createTaskResponse());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).runTask("test-app", "start app", "my-task", null, null);
    }

    @Test
    public void runTaskWithAllParameters() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_RUN_TASK);
        configurationMap.put(RUN_APPLICATION_NAME, "test-app");
        configurationMap.put(RUN_COMMAND, "start app");
        configurationMap.put(RUN_TASK_NAME, "my-task");
        configurationMap.put(RUN_MEMORY, "512");
        configurationMap.put(RUN_ENVIRONMENT, "FIRST=VALUE\nSECOND=THING");

        when(cloudFoundryService.runTask(anyString(), anyString(), anyString(), anyInt(), anyMap()))
                .thenReturn(createTaskResponse());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        Map<String, String> environment = new HashMap<>();
        environment.put("FIRST", "VALUE");
        environment.put("SECOND", "THING");
        verify(cloudFoundryService).runTask("test-app", "start app", "my-task", 512, environment);
    }

    @Test
    public void runTaskAndCaptureTaskId() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_RUN_TASK);
        configurationMap.put(RUN_APPLICATION_NAME, "test-app");
        configurationMap.put(RUN_COMMAND, "start app");
        configurationMap.put(RUN_TASK_NAME, "my-task");
        configurationMap.put(RUN_CAPTURE_TASK_ID, "true");
        configurationMap.put(RUN_TASK_ID_VARIABLE_NAME, "my-task-var");

        when(cloudFoundryService.runTask(anyString(), anyString(), anyString(), anyInt(), anyMap()))
                .thenReturn(createTaskResponse("some-id"));

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).runTask("test-app", "start app", "my-task", null, null);
        verify(variableContext).addResultVariable("my-task-var", "some-id");
    }

    @Test
    public void waitForTaskCompletion() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_WAIT_ON_TASK);
        configurationMap.put(WAIT_APPLICATION_NAME, "my-app");
        configurationMap.put(WAIT_TASK_ID, "1234");
        configurationMap.put(WAIT_TIMEOUT, "120");
        configurationMap.put(WAIT_FAIL_ON_TASK_FAILURE, "true");
        configurationMap.put(WAIT_TERMINATE_TASK_ON_TIMEOUT, "true");

        when(cloudFoundryService.waitForTaskCompletion("my-app", "1234", 120, true))
                .thenReturn(Mono.empty());

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).waitForTaskCompletion("my-app", "1234", 120, true);
    }

    @Test
    public void waitForTaskCompletionFailOnError() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_WAIT_ON_TASK);
        configurationMap.put(WAIT_APPLICATION_NAME, "my-app");
        configurationMap.put(WAIT_TASK_ID, "1234");
        configurationMap.put(WAIT_TIMEOUT, "1200");
        configurationMap.put(WAIT_FAIL_ON_TASK_FAILURE, "true");
        configurationMap.put(WAIT_TERMINATE_TASK_ON_TIMEOUT, "true");

        when(cloudFoundryService.waitForTaskCompletion("my-app", "1234", 1200, true))
                .thenReturn(Mono.error(new IllegalStateException("timeout")));

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
    }

    @Test
    public void waitForTaskCompletionDoNotFailOnError() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_WAIT_ON_TASK);
        configurationMap.put(WAIT_APPLICATION_NAME, "my-app");
        configurationMap.put(WAIT_TASK_ID, "1234");
        configurationMap.put(WAIT_TIMEOUT, "120");
        // not setting WAIT_FAIL_ON_TASK_FAILURE means false
        configurationMap.put(WAIT_TERMINATE_TASK_ON_TIMEOUT, "true");

        when(cloudFoundryService.waitForTaskCompletion(anyString(), anyString(), anyInt(), anyBoolean()))
                .thenReturn(Mono.error(new IllegalArgumentException("timeout")));

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).waitForTaskCompletion("my-app", "1234", 120, true);
    }

    @Test
    public void waitForTaskCompletionDoNotTerminateTaskOnTimeout() throws TaskException {
        // Given
        configurationMap.put(SELECTED_OPTION, OPTION_WAIT_ON_TASK);
        configurationMap.put(WAIT_APPLICATION_NAME, "my-app");
        configurationMap.put(WAIT_TASK_ID, "1234");
        configurationMap.put(WAIT_TIMEOUT, "120");
        configurationMap.put(WAIT_FAIL_ON_TASK_FAILURE, "true");
        configurationMap.put(WAIT_TERMINATE_TASK_ON_TIMEOUT, "false");

        when(cloudFoundryService.waitForTaskCompletion("my-app", "1234", 120, false))
                .thenReturn(Mono.error(new IllegalStateException("timeout")));

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
        verify(cloudFoundryService).waitForTaskCompletion("my-app", "1234", 120, false);
    }
}
